# Loaded by .git/hooks/(pre-commit|commit-msg|prepare-commit-msg)
# during git commit after local hooks have been installed.

hooks_chain_prepare_commit_msg="Utilities/Scripts/prepare-commit-msg"
hooks_chain_post_checkout="Utilities/Scripts/post-checkout"
hooks_chain_post_commit="Utilities/Scripts/post-commit"
hooks_chain_post_merge="Utilities/Scripts/post-merge"
hooks_chain_pre_push="Utilities/Scripts/pre-push"
