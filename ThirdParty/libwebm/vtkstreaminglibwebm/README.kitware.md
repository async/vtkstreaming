# libwebm fork for VTK

This branch contains changes required to embed libwebm into VTK. This
includes changes made primarily to the build system to allow it to be embedded
into another source tree.

  * Add attributes to pass commit checks within VTK.
  * Add CMake code to integrate with VTK's module system.
